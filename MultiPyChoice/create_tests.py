# These functions enable the creation of individual tests and a solution database
#     a custom number of question databases

import csv
import os
import random
from openpyxl import load_workbook
from openpyxl import Workbook
import time
import xml.etree.cElementTree as ET

def get_questions_texts(testbank, question_numbers):
    questions_texts = []
    for number in question_numbers:
        questions_texts.append(testbank[number])
    return questions_texts

def parse_questions_xml(file_name):
    tree = ET.ElementTree(file=file_name)
    root = tree.getroot()
    questions_list = []
    # for crappy formatting
    questions_list = []
    for item in root:
        #print(item.attrib['title'])
        text = item.attrib['title']
        question_type = item.attrib['question_type']
        score = item.attrib['score']
        for question in item:
            answers = []
            for data in question:
                try:
                    validity = data.attrib['valid']
                    if validity == 'valid':
                        answer = '+'
                except KeyError:
                    answer = '-'
                if data.text == '\n        ':        
                    for secsublevel in data:
                        #print(answer, secsublevel.text)
                        answers.append({'option': secsublevel.text, 'answer': answer})
                else:
                    answers.append({'option': data.text, 'answer': answer})
            #print()
            question = {'q': text, 'a': answers, 'type': question_type, 'score': score}
            if len(question['a']) > 3:
                questions_list.append(question)
    return questions_list

def parse_questions_properlyformatted_xml(file_name): # currently unused
    tree = ET.ElementTree(file=file_name)
    root = tree.getroot()
    questions_list = []
    # properly formatted case
    for item in root:
        #print(item.attrib['title'])
        text = item.attrib['title']
        for question in item:
            answers = []
            for data in question:
                try:
                    validity = data.attrib['valid']
                    if validity == 'valid':
                        answer = '+'
                except KeyError:
                    answer = '-'
                for secsublevel in data:
                    #print(answer, secsublevel.text)
                    answers.append({'option': secsublevel.text, 'answer': answer})
            question = {'q': text, 'a': answers}
            questions_list.append(question)
    return questions_list

def parse_questions_txt(infile):
    try:
        data = open(infile, 'r', encoding = "UTF-16")
    except:
        data_attempt = open(infile, 'r')
        data_attempt.readlines()
        data = open(infile, 'r')
    
    lines_list = []
    for line in data.readlines():
        #print(line)
        newline = line.strip().split('\n')
        lines_list.append(newline)
    lastline = len(lines_list)
    questions_list = []
    for line in range(len(lines_list)):
        if str(lines_list[line][0]).find("multi,score=") == True: # find headline of question    
            text = lines_list[line+1][0]
            answers = []
            if line + 7 > lastline:
                last_to_check = lastline - line
            else:
                last_to_check = 7
            for option_line in range(2,last_to_check):
                answer = lines_list[line+option_line][0][:1]
                option = lines_list[line+option_line][0][2:]
                if answer == '+' or answer =='-':
                    answers.append({'option': option, 'answer': answer})
            question = {'q': text, 'a': answers}
            questions_list.append(question)
    return questions_list

def parse_studentdata(workbook, extra_tests):
    wb = load_workbook(workbook)
    ws = wb.active
    students_list = []
    line = 1
    
    while True:
        if ws['A'+str(line)].value == "Students" or ws['A'+str(line)].value == "Hallgatók":
            firstline = line+2
            group = ws['B2'].value
            name_column = 'B'
            code_column = 'A'
            break
        if ws['B'+str(line)].value == "Name" or ws['B'+str(line)].value == "Név" or ws['B'+str(line)].value == "Vizsgaazonosító":
            group = ''
            firstline = line+1
            name_column = 'B'
            code_column = None
            break
        if ws['A'+str(line)].value == "Name" or ws['A'+str(line)].value == "Név":
            group = ''
            firstline = line + 1
            name_column = 'A'
            code_column = 'B'
            break
        if line > 100:
            print('No students found.')
            return None
        line = line + 1
    line = firstline

    while True:
        name_index = name_column+str(line)
        if code_column != None:
            code_index = code_column+str(line)
        else:
            code_index = None
            
        name = ws[name_index].value
        if name!= None:
            if code_index != None:
                if ws[code_index].value != None:
                    st_code = ws[code_index].value
                else:
                    st_code = ''
            else:
                st_code = ''
            new_student = {'name': name, 'code': st_code, 'group': group}
            students_list.append(new_student)
            line = line + 1
        else:
            break
    
    for extra in range(extra_tests):
        extra_student = {'name': 'Extra '+str(extra+1), 'code': '', 'group': ''}
        students_list.append(extra_student)
    print(students_list)
    return students_list

def convert_xml_to_txt(xml_file, outfile_name = None):
    ok = 0
    fixed_string = "﻿[multi,score=4,random=1,layout=2]"
    print('Use this to test the functions')
    files = [xml_file]
    for file in files:
        questions = parse_questions_xml(file)
        lines = []
        for question in questions:
            if len(question['a']) > 3 and question['type'] == '2':
                ok  = ok+1
                lines.append('[multi,score='+str(int(float(question['score'])))+',random=1,layout=2]')
                lines.append(question['q'])
                for answer in question['a']:
                    answerline = answer['answer']+' '+answer['option']
                    lines.append(answerline)
                lines.append('')
        if outfile_name is not None:
            outfile = open(outfile_name+'-cs.txt','w', encoding="UTF-16")
        else:
            outfile = open(file+'-cs.txt','w', encoding="UTF-16")
        for line in lines:
            print(line)
            outfile.write(line + '\n')
        outfile.close()

if __name__  == "__main__":
#    print('Use this to test the functions')
    questions=parse_questions_txt('/home/tamas/Projects/MultiPyChoice/MultiPyChoice_v0.0.8/questions/sample_1.txt')
    print(questions)