#!/bin/bash
python3 -m pip install colorama==0.4.4
python3 -m pip install et-xmlfile==1.1.0
python3 -m pip install fpdf==1.7.2
python3 -m pip install imutils==0.5.4
python3 -m pip install numpy==1.21.4
python3 -m pip install opencv-python==4.5.4.58
python3 -m pip install openpyxl==3.0.9
python3 -m pip install pdf2image==1.16.0
python3 -m pip install Pillow==8.4.0
python3 -m pip install pip==21.3.1
python3 -m pip install PyPDF2==1.26.0
python3 -m pip install PySimpleGUI==4.55.1
python3 -m pip install qrcode==7.3.1
python3 -m pip install scipy==1.7.2
python3 -m pip install setuptools==57.4.0
python3 -m pip install matplotlib==3.5.0
