# These functions enable the creation of the empty test sheets

import os
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import qrcode
from PyPDF2 import PdfFileMerger
import openpyxl
import matplotlib.font_manager as fm
from give_empty_image import *


def generate_qr(data):
    qr = qrcode.QRCode(version=1, box_size=10, border=0)
    qr.add_data(data)
    qr.make()
    img = qr.make_image(fill_color="black", back_color="white")
    return img

def create_page(empty_img, full_id, hashed_id, test_lang, test_len, options_len):
    #print(full_id)
    img_qr = generate_qr(hashed_id)
    if empty_img == None:
        img1 = give_empty_image(test_lang)
        
    draw = ImageDraw.Draw(img1)
    # xy = [(x0, y0), (x1, y1)]
    # cover unneeded answer numbers and their corresponding circles
    lower_limit = (1430, 2130)
    x0 = 800
    
    # determine length for bubble lines (in groups of 5)
    if test_len <= 60: bubbles_len = 60
    if test_len <= 55: bubbles_len = 55
    if test_len <= 50: bubbles_len = 50
    if test_len <= 45: bubbles_len = 45
    if test_len <= 40: bubbles_len = 40
    if test_len <= 35: bubbles_len = 35  
    if test_len <= 30: bubbles_len = 30
    
    # length_pairs definition: (test_length, lower bound for the covering rectangle)
    length_pairs = [(30, 460),(35,758),(40,1035),(45,1315),(50,1583),(55,1849),(60,2145)]
    for pair in length_pairs:
        if bubbles_len == pair[0]:
            y0 = pair[1]
            break
        else:
            if test_len > 60: y0 = 2030
            else: y0 = 338
    upper_limit = (x0,y0)
    draw.rectangle([upper_limit, lower_limit], fill=(255,255,255), outline=None, width=1)
    
    if options_len < 5:
        draw.rectangle([(650,450), (710,2140)], fill=(255,255,255), outline=None, width=1)
        draw.rectangle([(1350,450), (1440,2140)], fill=(255,255 ,255), outline=None, width=1)
    
    img_qr = img_qr.resize((350,350))
    img1.paste(img_qr, (1100,45))
    draw = ImageDraw.Draw(img1)
    font = ImageFont.truetype(fm.findfont(fm.FontProperties(family="sans-serif", weight ="semibold")),30)
    name, code, group, test_topic, attempt, date = full_id[0], full_id[1], full_id[2], full_id[3], full_id[4], full_id[5]
    full_id = name
        
    firstline = name + ' '+ code
    secondline = ''
    if group == '':
        secondline = test_topic + ' (' + attempt +')'
        thirdline = date
        fourthline = ''
    else:
        if test_lang == 'hu':
            secondline = str(group) + '. csoport'
        if test_lang == 'en':
            secondline = 'Group ' + str(group)
        thirdline = test_topic + ' (' + attempt +')'
        fourthline = date
    
    draw.text((97, 70), firstline, (0,0,0), font = font)
    draw.text((97, 140), secondline, (0,0,0), font = font)
    draw.text((97, 210), thirdline, (0,0,0), font = font)
    draw.text((97, 290), fourthline, (0,0,0), font = font)
    
    pdf_out = img1.convert('RGB')
    out_pdf_name = name+'_'+attempt+'.pdf'
    pdf_out.save(out_pdf_name, resolution = 191)
    return out_pdf_name

if __name__  == "__main__":
    print('Use this to test the functions')