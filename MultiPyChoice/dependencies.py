from pip._internal import main as pipmain

for package in 'scipy', 'pypdf2', 'qrcode', 'opencv-python', ' pdf2image', ' imutils', ' numpy', ' pillow', 'openpyxl', ' pysimplegui', ' fpdf':
    pipmain(['install', package])
