import PySimpleGUI as sg
from create_tests import *
from pathlib import Path
folder_testfolders = chosen_testfolders = []

sg.ChangeLookAndFeel('Reddit')
# TEST join layout
column_sj1 = [
        [sg.Text('Mappa tartalma')],
        [sg.Listbox(values=folder_testfolders, size=(40,20), key='-FOLDER-TESTFOLDERS-',
                     bind_return_key=True)]]

column_sj2 = [
        [sg.Text('Kiválasztott mappák')],
        [sg.Listbox(values=chosen_testfolders, size=(40,20), key='-CHOSEN-TESTFOLDERS-',
                    bind_return_key=True)]]

column_sj3 = [
    [sg.Frame('Lehetőségek',[
        [sg.Button('Összes kiválasztása', key = '-SELECTALL-TESTFOLDERS-',button_color=('white', 'blue'), size=(16,1))],
        [sg.Button('Kiválasztás elfelejtése', key = '-RESET-TESTFOLDERS-',button_color=('white', 'red'), size=(16,1))]])]
    ]

window_layout = [
    [sg.Frame('Mappaválasztás',
    [[sg.Text('Exportált mappákat tartalmazó mappa kiválasztása (nem egyes fájlok!)')],
        [sg.InputText(os.getcwd(),key='-TESTDIR-', size=(90,1)), sg.FolderBrowse('Választás', enable_events = True),
     sg.Button('Betöltés', key = '-TESTDIR-UPDATE-')],
    ]
              )],
    [sg.Column(column_sj1),sg.Column(column_sj2), sg.Column(column_sj3)],
    [sg.Text()],
    [sg.Button('Konvertálás', key = '-CONVERT-', button_color=('white', 'green'),size=(12,1))]]

window = sg.Window('TestConverter v0.0.1', window_layout)
while True:
    event, values = window.read(timeout=100)
    
    if event in (sg.WIN_CLOSED, 'Exit'):
        break
    
    elif event == '-TESTDIR-UPDATE-':
        folder_testfolders = []
        for folder in os.listdir(values['-TESTDIR-']):
            if Path(values['-TESTDIR-']+'/'+folder).is_file():
                pass
            else:
                folder_testfolders.append(folder)
        folder_testfolders.sort()
        chosen_testfolders = []
        window['-FOLDER-TESTFOLDERS-'].update(folder_testfolders)
        window['-CHOSEN-TESTFOLDERS-'].update(chosen_testfolders)
        
    
    elif event == '-FOLDER-TESTFOLDERS-':
        if folder_testfolders != []:
            chosen_testfolders.append(values['-FOLDER-TESTFOLDERS-'][0])
            window['-CHOSEN-TESTFOLDERS-'].update(values=chosen_testfolders)

    elif event == '-CHOSEN-TESTFOLDERS-':
        try:
            chosen_testfolders.remove(values['-CHOSEN-TESTFOLDERS-'][0])
        except IndexError:
            pass
        window['-CHOSEN-TESTFOLDERS-'].update(values=chosen_testfolders)
    
    elif event == '-SELECTALL-TESTFOLDERS-':
        for folder in folder_testfolders:
                chosen_testfolders.append(folder)
        window['-CHOSEN-TESTFOLDERS-'].update(chosen_testfolders)
        
    elif event == '-RESET-TESTFOLDERS-':
        chosen_testfolders = []
        window['-CHOSEN-TESTFOLDERS-'].update(chosen_testfolders)
    
    elif event == '-CONVERT-':
        basefolder = values['-TESTDIR-']
        for folder in chosen_testfolders:
            os.chdir(basefolder+'/'+folder)
            if Path('quizitems.xml').is_file():
                convert_xml_to_txt('quizitems.xml', folder)
            
