# import dependencies
import os
import numpy as np
import csv
import PySimpleGUI as sg
import openpyxl
from hashlib import md5 as hashlib_md5
from fpdf import FPDF
from datetime import date
from pdf2image import convert_from_path
import shutil
import subprocess
import matplotlib.font_manager as fm
import time
from pathlib import Path

# import parts of the script
from config import *
from create_sheets import *
from image_identification import *
from choice_recognition import *
from show_images import *
from create_tests import *
from test_compositions import *

__version__ = '0.1.0'


def read_pages(location, settings, poppler_path):
    contrast = settings['enhance_png_contrast']
    page_counter = 0
    ingested_pages = []
    pdf_list = []
    image_list = []
    pages = None
    for file in os.listdir():
        if (str(file)[-4:]).lower() == ".pdf":
            pdf_list.append(file)
         
    for file in pdf_list:       
        if sys.platform == "win32":
            try:
                pages = convert_from_path(file, settings['png_ppi'], poppler_path = poppler_path)
            except:
                cmd = subprocess.Popen('magick convert "'+file+'" page_%d.jpg')
                cmd.communicate()
                #os.popen('magick convert "'+file+'" page_%d.jpg')
                #for second in range(10,0,-1):
                #    print(second)
                #    time.sleep(1)
                #print()
        else:
            pages = convert_from_path(file, settings['png_ppi'])
        
        if pages != None:
            for page in pages:
                page.save('out.png', 'PNG')
                lowcon = Image.open('out.png')
                if contrast == 0:
                    lowcon.save('scanned_image_'+str(page_counter)+'.png')
                else: 
                    enh_con = ImageEnhance.Contrast(lowcon)
                    image_contrasted = enh_con.enhance(contrast) # might help with grading but breaks some QR codes
                    image_contrasted.save('scanned_image_'+str(page_counter)+'.png')
                ingested_pages.append('scanned_image_'+str(page_counter)+'.png')
                page_counter = page_counter + 1
            
    # if poppler is not available, imagemagic will give jpg's - need to check after conversion
    for file in os.listdir():    
        if (str(file)[-4:]).lower() == ".jpg":
            image_list.append(file)
            
    for file in image_list:
        lowcon = Image.open(file)
        #lowcon = lowcon.transpose(method=Image.ROTATE_180)
        if contrast == 0:
            lowcon.save('scanned_image_'+str(page_counter)+'.png')
        else: 
            enh_con = ImageEnhance.Contrast(lowcon)
            image_contrasted = enh_con.enhance(contrast) # might help with grading but breaks some QR codes
            image_contrasted.save('scanned_image_'+str(page_counter)+'.png')
        ingested_pages.append('scanned_image_'+str(page_counter)+'.png')
        page_counter = page_counter + 1
    
    return ingested_pages

def ingest_solution(solution_database, salt):
    try:
        if os.path.isfile(solution_database):
            solution_list = list(csv.reader(open(solution_database, 'r', encoding = "UTF-16"), delimiter=','))
            students = []
            for line in range(len(solution_list)):
                if solution_list[line][0] == '[BEGIN]':
                    full_id = solution_list[line+1]
                    try:
                        name, code, group, topic, attempt, date = full_id[0], full_id[1], full_id[2], full_id[3], full_id[4], full_id[5]
                    except IndexError:
                        print('Index error, full_id is used as name')
                        name = full_id
                        group = ''
                        date = ''
                        code = ''
                    hashed_id = hashlib_md5(bytes(salt+str(full_id), 'utf-8')).hexdigest()
                    start_solution = line+2
                if solution_list[line][0] == '[END]':
                    test_length = int(solution_list[line-1][0])
                    students.append({'test_id': full_id, 'hashed_id': hashed_id, 'name': name, 'code': code,'group': group, 'date': date ,'begin_line': start_solution, 'test_len': test_length, 'attempt': attempt,'topic': topic})
            return solution_list, students
        else:
            return [], []
    except ValueError:
        return [], []

def check_grading(grade2,grade3,grade4,grade5):
    if grade2 < grade3:
        if grade3 < grade4:
            if grade4 < grade5:
                return True
            else: return False
            
def give_grade(score,maxscore,grade2,grade3,grade4,grade5):
    percent = 100 * score / maxscore
    if percent >= grade5:
        return 5
    if percent >= grade4:
        return 4
    if percent >= grade3:
        return 3
    if percent >= grade2:
        return 2
    else:
        return 1

def export_grades(students, outfile):
    wb = openpyxl.Workbook()
    sheet = wb.active
    sheet.title = 'Grades'
    wb.create_sheet(index = 1, title = 'Errors')
    wb['Grades']['A1'] = 'Topic'
    wb['Grades']['B1'] = 'Name'
    wb['Grades']['C1'] = 'Group'
    wb['Grades']['D1'] = 'Grade'
    wb['Grades']['E1'] = 'Score'
    topiccol = 'A'
    namecol = 'B'
    groupcol = 'C'
    gradecol ='D'
    scorecol = 'E'
    gline = 2
    eline = 1
    for i in range(len(students)):
        errors = True
        try:
            grade = students[i]['grade']
            if students[i]['errors'] == []:
                errors = False
        except KeyError:
            grade = False
        if grade != False:
            wb['Grades'][topiccol+str(gline)] = students[i]['topic']+' ('+students[i]['attempt']+')'
            wb['Grades'][namecol+str(gline)] = students[i]['name']
            wb['Grades'][groupcol+str(gline)] = students[i]['group']
            wb['Grades'][gradecol+str(gline)] = students[i]['grade']
            wb['Grades'][scorecol+str(gline)] = students[i]['score']
            gline = gline+1
            if errors != False:
                wb['Errors']['A'+str(eline)] = students[i]['name']
                wb['Errors']['B'+str(eline)] = students[i]['topic']
                eline = eline + 1
                for k in range(len(students[i]['errors'])):
                    qnumber = students[i]['errors'][k][0][0]
                    correct = ''
                    given = ''
                    for c in range(1,5):
                        correct = correct + students[i]['errors'][k][0][c] + ' '
                    for g in range(0,4):
                        given = given + students[i]['errors'][k][1][g] + ' '
                    wb['Errors']['A'+str(eline)] = qnumber
                    wb['Errors']['B'+str(eline)] = correct
                    wb['Errors']['C'+str(eline)] = given
                    eline = eline + 1                
    outfile_name = outfile+'.xlsx'
    wb.save(outfile_name)
    return outfile_name

def create_answer_sheets(solution_file, test_lang, salt, questions_pdf_list=None):
    if questions_pdf_list == None:
        skip_questions = True
    else:
        skip_questions = False
        
    solution_list, students = ingest_solution(solution_file, salt)
    
    if solution_list == [] or students == []:
        return None
    else:            
        empty_page = None # insert an actual png here, if you want to use your own
        attempt = students[0]['attempt']
        merge_list = []
        for student in range(len(students)):
            student_pdf = []
            if skip_questions == False:
                student_pdf.append(questions_pdf_list[student])
            answersheet_pdf = create_page(empty_page, students[student]['test_id'], students[student]['hashed_id'], test_lang, students[student]['test_len'], options_len=4)
            student_pdf.append(answersheet_pdf)
            merge_list.append(student_pdf)
       
        empty_back = FPDF(format='A4')
        empty_back.output('empty_back.pdf', 'F')
        merged_files = []
        for student in merge_list:
            merger = PdfFileMerger()
            merger.append(student[0])
            merger.append(student[1])
            merger.append('empty_back.pdf')
            merged_name = 'TEST_'+(str(student[1])[:-4])+'.pdf'
            merger.write(merged_name)
            merged_files.append(merged_name)
            merger.close()
        for file in merge_list:
            try:
                os.remove(file[0])
                os.remove(file[1])
            except:
                pass
        try:
            os.remove('empty_back.pdf')
        except:
            pass
        
        # changes
        merger = PdfFileMerger()
        for file in merged_files:
            merger.append(file)
        merger.write('TEST-PRINT_ALL.pdf')
        merger.close()
        
        # changes
        
        return merged_files

def main():
    # Intiation
    script_location = os.getcwd()
    poppler_path = script_location + '\\poppler-21.11.0\\Library\\bin'
    config = get_config()
    chosen_printfiles = folder_printfiles = filtered_printfiles = []
    chosen_solutionfiles = folder_solutionfiles =[]
    locale = config['localization']
    if locale == 'hu':
        local_text = localize_hu() # more languages can be added with elif statements
    else:
        local_text = localize_en()
        
    sg.ChangeLookAndFeel('Reddit')
    qlines = 0
    compositions = test_compositions()
    composition_names = []
    for composition in compositions:
        composition_names.append(composition[locale])

    # Evaluation tab layout
    columnev1 = [        
        [sg.Frame(local_text['evaluation']['files'],[
        [sg.Text(local_text['evaluation']['solutions'], auto_size_text=False,justification='left')],
        [sg.InputText(os.getcwd(), key='-EVAL-SOLUTIONDB-',size=(90,1)),sg.FileBrowse(local_text['browsebutton'])],
        [sg.Text(local_text['evaluation']['scanfolder'], auto_size_text=False, justification='left')],
        [sg.InputText(os.getcwd(), key='-WDIR-', size=(90,1)), sg.FolderBrowse(local_text['browsebutton'])],
        [sg.Text(local_text['evaluation']['exportfile_text'])],
        [sg.InputText(local_text['evaluation']['exportfile_suggest']+'_'+str(date.today()), key='-OUTSCORE-FILE-',size=(90,1)), sg.Text('.xlsx')] 
        ])]
        ]
    ev_percent_column = [
        [sg.InputText(config['grading']['percent_to_grade'][2], key = 'GRADE-2', size=(4,1))],
        [sg.InputText(config['grading']['percent_to_grade'][3], key = 'GRADE-3', size=(4,1))],
        [sg.InputText(config['grading']['percent_to_grade'][4], key = 'GRADE-4', size=(4,1))],
        [sg.InputText(config['grading']['percent_to_grade'][5], key = 'GRADE-5', size=(4,1))]
        ]
    
    ev_marks_column = [
        [sg.Text(local_text['evaluation']['2'], size=(12,1))],
        [sg.Text(local_text['evaluation']['3'], size=(12,1))],
        [sg.Text(local_text['evaluation']['4'], size=(12,1))],
        [sg.Text(local_text['evaluation']['5'], size=(12,1))]
        ]
    
    ev_options_column = [
        [sg.Text(local_text['evaluation']['correct_points'])],
        [sg.Text(local_text['evaluation']['incorrect_points'])],
        [sg.Text(local_text['evaluation']['partial_points'])]
        ]
    
    ev_values_column = [
        [sg.InputText(config['grading']['correct_points'],key='-CORRECTPOINTS-', size=(4,1))],
        [sg.InputText(config['grading']['incorrect_points'], key='-INCORRECTPOINTS-', size=(4,1))],
        [sg.Checkbox('',default=config['grading']['partial_points'], key='-PARTIALPOINTS-')]
        ]
    
    columnev2 = [
        [sg.Frame(local_text['evaluation']['gradingframe'],[
            [sg.Column(ev_marks_column), sg.Column(ev_percent_column)]]),
        sg.Frame(local_text['evaluation']['settingsframe'],[
            [sg.Column(ev_options_column), sg.Column(ev_values_column)]])]
        ]
         
    evaluation_layout = [[sg.Column(columnev1)],[sg.Column(columnev2)],
                         [sg.Text()],
                         [sg.Button(local_text['evaluation']['evalbutton'], key ='-EVALUATE-', size=(12,1),button_color=('white', 'green'), disabled = False),
                          sg.Button(local_text['evaluation']['exportbutton'], key = '-EXPORT-', size=(12,1),button_color=('white', 'blue'), disabled = True)]]
    
    
    # Print tab layout
    column_pr1 = [
            [sg.Text(local_text['print']['files_wdir'])],
            [sg.Listbox(values=folder_printfiles, size=(40,20), key='-FOLDER-PRINTFILES-',
                         bind_return_key=True)]]
   
    column_pr2 = [
            [sg.Text(local_text['print']['files_chosen'])],
            [sg.Listbox(values=chosen_printfiles, size=(40,20), key='-CHOSEN-PRINTFILES-',
                        bind_return_key=True)]]
    
    column_pr3 = [
        [sg.Frame(local_text['print']['filterframe'],[
        [sg.Button(local_text['print']['filterbutton']+'"1"', key = '-FILTER1-', size=(12,1))],
         [sg.Button(local_text['print']['filterbutton']+'"2"', key = '-FILTER2-', size=(12,1))],
         [sg.Button(local_text['print']['filterbutton']+'"3"', key = '-FILTER3-', size=(12,1))],
         [sg.Button(local_text['print']['filterbutton']+'"4"', key = '-FILTER4-', size=(12,1))],
         [sg.Text('')],
         [sg.Button(local_text['print']['addbutton'], key = '-FILTERADD-',button_color=('white', 'green'), size=(12,1))],
         [sg.Button(local_text['print']['filteroff'], key = '-FILTEROFF-',button_color=('white', 'orange'), size=(12,1))]
        ])],
        [sg.Frame(local_text['print']['resetframe'],[
            [sg.Button(local_text['print']['resetbutton'], key = '-RESET-PRINTFILES-',button_color=('white', 'red'), size=(12,1))]])]
        ]
    
    print_layout = [
        [sg.Frame(local_text['print']['folderframe'],
        [[sg.Text(local_text['print']['folderframe_text'])],
            [sg.InputText(os.getcwd(),key='-PRINTDIR-', size=(90,1)), sg.FolderBrowse(local_text['browsebutton'], enable_events = True),
         sg.Button(local_text['print']['loadbutton'], key = '-PRINTDIR-UPDATE-')]])],
        [sg.Column(column_pr1),sg.Column(column_pr2), sg.Column(column_pr3)],
        [sg.Text()],
        [sg.Button(local_text['print']['printbutton'], key = '-PRINT-START-', button_color=('white', 'green'),size=(12,1))]]

    
    # Creation tab layout  
    columncr1 = [
        [sg.Frame(local_text['testcreation']['studentframe'],[
        [sg.Text(local_text['testcreation']['students'], auto_size_text=False,justification='left')],
        [sg.InputText(os.getcwd(), key='-STUDENTS-WS-',size=(90,1)),sg.FileBrowse(local_text['browsebutton'])]
        ])],
        
        [sg.Frame(local_text['testcreation']['testframe'],[
        [sg.Text(local_text['testcreation']['premade'])],
        [sg.Combo(values=composition_names, readonly=True, k='-COMPCOMBO-', enable_events = True)],
        [sg.Text(local_text['testcreation']['newsolution_text'])],
        [sg.InputText(local_text['testcreation']['newsolution_suggest']+'_'+str(date.today()), key='-SOLUTIONS-NEWOUTFILE-',size=(40,1)), sg.Text('.txt')],
        [sg.Text(local_text['testcreation']['topic'], auto_size_text=False,justification='left')],
        [sg.InputText(' ', size=(40,1), key = '-TEST-TOPIC-')],
        [sg.Text(local_text['testcreation']['attempts'], auto_size_text=False,justification='left')],
        [sg.Combo(values=(1,2,3,4), default_value=1, readonly=True, k='-ATTEMPTCOMBO-', enable_events = True)],
        [sg.Text(local_text['testcreation']['extratests'])],
        [sg.Slider(range = (0,100), key = '-EXTRATESTS-', default_value = 0,resolution = 1, orientation = 'h', size=(34,12))]
            ])],
        
        [sg.Frame(local_text['testcreation']['language'],[
                  [sg.Radio(local_text['testcreation']['hu'],"RADIO1", default = True, key = 'LANG-HU')],
                  [sg.Radio(local_text['testcreation']['en'],"RADIO1", key = 'LANG-EN')]
                  ]),
         sg.Frame(local_text['testcreation']['groupquestions'],[
                  [sg.Radio(local_text['testcreation']['yes'],"RADIO2", key = '-GROUP-Q-')],
                  [sg.Radio(local_text['testcreation']['no'], "RADIO2", default = True)]
                  ])
         ]]
    
    columncr2 = []
    
    creation_layout = [[sg.Column(columncr1), sg.Column(columncr2)],
                       [sg.Text('')],
                       [sg.Button(local_text['testcreation']['createbutton'], key = '-CREATE-', size=(12,1),button_color=('white', 'green'), disabled = False),
                    ]]
    
    
    questions_layout = [
        [sg.Frame('',[
        [sg.Frame(local_text['questions']['db']+str(qlines+1),[
        [sg.Slider(range = (0,60), key = '-QNO0-', default_value = 5,resolution = 1, orientation = 'h', size=(20,8)),
         sg.InputText(os.getcwd(), key='-QDB0-',size=(70,8)),sg.FileBrowse(local_text['browsebutton'])],
        ])]], key = 'QFRAME')],
        [sg.Button(local_text['questions']['addbutton'], key = '-ADD-QLINE-', size=(12,1),button_color=('white', 'blue'))]]
    
    
    columnjs1 = [
    [sg.Frame(local_text['justsheets']['solution'],[
    [sg.Text(local_text['testcreation']['prevsolution'], auto_size_text=False,justification='left')],
    [sg.InputText(os.getcwd(), key='-STUDENTS-WS-',size=(90,1)),sg.FileBrowse(local_text['browsebutton'])]])
    ]]

    columnjs2 = []
    
    
    justsheets_layout = [[sg.Column(columnjs1), sg.Column(columnjs2)],
                       [sg.Text('')],
                       [sg.Button(local_text['testcreation']['sheetsbutton'], key = '-JUST-SHEETSDB-', size=(12,1),button_color=('white', 'BLUE'))]]
    
    
    # Solution join layout
    column_sj1 = [
            [sg.Text(local_text['solution_join']['files_wdir'])],
            [sg.Listbox(values=folder_printfiles, size=(40,20), key='-FOLDER-SOLUTIONFILES-',
                         bind_return_key=True)]]
   
    column_sj2 = [
            [sg.Text(local_text['solution_join']['files_chosen'])],
            [sg.Listbox(values=chosen_printfiles, size=(40,20), key='-CHOSEN-SOLUTIONFILES-',
                        bind_return_key=True)]]
    
    column_sj3 = [
        [sg.Frame(local_text['solution_join']['selectframe'],[
            [sg.Button(local_text['solution_join']['selectallbutton'], key = '-SELECTALL-SOLUTIONFILES-',button_color=('white', 'blue'), size=(16,1))],
            [sg.Button(local_text['solution_join']['resetbutton'], key = '-RESET-SOLUTIONFILES-',button_color=('white', 'red'), size=(16,1))]])]
        ]
    
    solution_join_layout = [
        [sg.Frame(local_text['solution_join']['folderframe'],
        [[sg.Text(local_text['solution_join']['folderframe_text'])],
            [sg.InputText(os.getcwd(),key='-SOLUTIONDIR-', size=(90,1)), sg.FolderBrowse(local_text['browsebutton'], enable_events = True),
         sg.Button(local_text['solution_join']['loadbutton'], key = '-SOLUTIONDIR-UPDATE-')],
        [sg.Text(local_text['solution_join']['joinedname_text'])],
        [sg.InputText(local_text['solution_join']['joinedname_suggest']+str(date.today()),key='-SOLUTION-JOINED-FILE-', size=(90,1)), sg.Text('.txt')]]
                  )],
        [sg.Column(column_sj1),sg.Column(column_sj2), sg.Column(column_sj3)],
        [sg.Text()],
        [sg.Button(local_text['solution_join']['joinbutton'], key = '-SOLUTIONJOIN-', button_color=('white', 'green'),size=(12,1))]]
    
    
    # Plots tab layout
    plots_layout = [[sg.Text(local_text['plots']['message'])]]
    
    
    # Complete window layout
    window_layout = [
        [sg.Text(local_text['salt'], size=(18, 1), auto_size_text=False,justification='left'), sg.InputText('', size=(20,1), key = '-SALT-')],
        [sg.Text()],
        [sg.TabGroup([[sg.Tab(local_text['evaluation']['title'], evaluation_layout),
                       sg.Tab(local_text['print']['title'], print_layout),
                       sg.Tab(local_text['testcreation']['title'], creation_layout),
                       sg.Tab(local_text['questions']['title'], questions_layout),
                       sg.Tab(local_text['justsheets']['title'], justsheets_layout),
                       sg.Tab(local_text['solution_join']['title'], solution_join_layout),
                       #sg.Tab(local_text['plots']['title'], plots_layout)
                       ]]),
         #sg.Output(size=(60,10))
         ]
        ]
    
    window = sg.Window('MultiPyChoice v'+__version__, window_layout)
    
    while True:
        event, values = window.read(timeout=100)
        if event in (sg.WIN_CLOSED, 'Exit'):
            break

        elif event == '-EVALUATE-':
            correct_points = float(values['-CORRECTPOINTS-'])
            incorrect_points = float(values['-INCORRECTPOINTS-'])
            if check_grading(int(values['GRADE-2']), int(values['GRADE-3']), int(values['GRADE-4']), int(values['GRADE-5'])) == True:
                print(local_text['success']['processing_images'])
                print()
                sg.popup_animated(sg.DEFAULT_BASE64_LOADING_GIF, background_color='white', time_between_frames=100)
                grades_given = 0
                os.chdir(values['-WDIR-'])
                location = ''
                pages = read_pages(location, config['image_processing'], poppler_path)
                solution_file = values['-EVAL-SOLUTIONDB-']
                solution_list, students = ingest_solution(solution_file, str(values['-SALT-']))
                #print(students)
                solution_error = False
                if solution_list == [] or students == []:
                    print(local_text['errors']['solution_notparsed'])
                    solution_error = True
                
                if solution_error == False:
                    grading_success = False
                    identified_pages = []
                    for page in range(len(pages)):
                        # Identifying the test and the orientation of the scanned page
                        paper_id, rotated = identify_test(pages[page])
                        
#                         # Setting orientation removed
#                         if rotated:
#                             img = cv2.imread(pages[page])
#                             img = cv2.rotate(img, cv2.cv2.ROTATE_180)
#                         else:
#                             img = cv2.imread(pages[page])

                        img = cv2.imread(pages[page])

                        identified_pages.append({'image': img, 'id': paper_id, 'file': pages[page]})
                        
                    manual_notification = True
                    sg.popup_animated(None)
                    for page in range(len(identified_pages)):
                        # Identifying pages manually
                        if identified_pages[page]['id'] == '':
                            if manual_notification == True:
                                sg.popup(local_text['manual_id']['popup_info'])
                                manual_notification = False
                            man_paper_id, rotated = identify_test_manual(pages[page], students, local_text['manual_id'])
                            identified_pages[page]['id'] = man_paper_id
                            #identified_pages[page]['image'] = cv2.imread(pages[page])
                    
                    for page in range(len(identified_pages)):
                        img = identified_pages[page]['image']
                        paper_id = identified_pages[page]['id']
                        
                        if paper_id != '':
                            # Reading answers
                            decode_success, pixel_array = handle_image(img)
                            if decode_success == True:
                                answers = find_choices(pixel_array, config['image_processing']['answer_treshold'])
                                counter = 1
                                answers_readable = []
                                for line in range(len(answers)):
                                    if line % 6 != 0:
                                        answers_readable.append(answers[line-1])
                                        counter = counter + 1
                                id_matched = False
                                
                                for student in range(len(students)):
                                    if paper_id == students[student]['hashed_id']:
                                        current_student = students[student]
                                        student_line_to_print = str(local_text['success']['student']) +' '+str(current_student['name'])
                                        if current_student['group'] != '':
                                            student_line_to_print = student_line_to_print +', Group'+' '+str(current_student['group'])
                                        print(student_line_to_print)
                                        line_offset = current_student['begin_line']
                                        id_matched = True
                                        break
                                
                                if id_matched == True:
                                    score = 0.0
                                    error_lines = []
                                    maxscore = 0
                                    for line in range(current_student['test_len']):
                                        error_in_line = False
                                        this_line_score = 0.0
                                        for circle in range(0,len(solution_list[line+line_offset])-1):
                                            if solution_list[line+line_offset][circle+1] == answers_readable[line][circle]:
                                                this_line_score = this_line_score + correct_points
                                                maxscore = maxscore + correct_points
                                            else:
                                                this_line_score = this_line_score + incorrect_points
                                                maxscore = maxscore + correct_points
                                                error_in_line = True
                                        print(solution_list[line+line_offset], answers_readable[line])
                                        if error_in_line == True:
                                            error_lines.append([solution_list[line+line_offset], answers_readable[line]])
                                            if values['-PARTIALPOINTS-'] == False:
                                                this_line_score = 0.0
                                        score = score + this_line_score
                                    students[student]['score'] = score
                                    grade = give_grade(score, maxscore, int(values['GRADE-2']),int(values['GRADE-3']),
                                                       int(values['GRADE-4']),int(values['GRADE-5']))
                                    students[student]['grade'] = grade
                                    students[student]['errors'] = error_lines
                                    print(local_text['success']['score'], score, local_text['success']['grade'], grade)
                                    grades_given = grades_given + 1
                                    grading_success = True
                                    if grading_success == True:
                                        if 'Extra ' in students[student]['name']:
                                            for char in students[student]['name'][-3:]:
                                                if char.isdigit():    
                                                    manual_name = extra_to_name_manual(identified_pages[page]['file'], students, local_text['manual_extra'])
                                                    if manual_name != '':
                                                        students[student]['name'] = manual_name+' ('+students[student]['name']+')'
                                                    break
                                    print()
                                else:
                                    print(identified_pages[page]['file'], local_text['errors']['missing'])
                                    print()
                            else:
                                print(identified_pages[page]['file'], local_text['errors']['cv'])
                                print()
                        else:
                            print(identified_pages[page]['file'], local_text['errors']['qr'])
                            print()
                    if grading_success == True:
                        window['-EXPORT-'].update(disabled = False)
            print(str(grades_given)+'/'+str(len(students)), local_text['success']['evaluated'])
            print()
            
        elif event == '-EXPORT-':
            if students != []:
                outfile = values['-OUTSCORE-FILE-']
                outfile_name = export_grades(students, outfile)
                print(local_text['success']['export'])
                print(values['-WDIR-']+'/'+outfile_name)
                print()

        elif event == '-COMPCOMBO-':
            for composition in compositions:
                if composition[locale] == values['-COMPCOMBO-']:
                    test_composition = composition
                    break
            catlines = 0
            for line in range(qlines):
                window[f'-QNO{line}-'].update(0)
                
            for category in composition['categories']:
                if catlines > qlines:
                    qlines = qlines + 1
                    window.extend_layout(window['QFRAME'],[
                        [sg.Frame(local_text['questions']['db']+str(qlines+1),[
                        [sg.Slider(range = (0,60), key = f'-QNO{qlines}-', default_value = 20,resolution = 1, orientation = 'h', size=(20,8)),
                         sg.InputText(os.getcwd()+'', key= f'-QDB{qlines}-',size=(70,8)),sg.FileBrowse(local_text['browsebutton'])]])]
                        ])
                window[f'-QDB{catlines}-'].update(category['file'])
                window[f'-QNO{catlines}-'].update(category['number'])
                catlines = catlines + 1
            if composition['language'] == 'hu':
                window['LANG-HU'].update(True)
            else:
                window['LANG-EN'].update(True)
            
            window['-SOLUTIONS-NEWOUTFILE-'].update(composition['topic']+'_'+str(date.today()))
            window['-TEST-TOPIC-'].update(composition['topic'])
       
        
        elif event == '-CREATE-':
            # Creating solution file, and then creating answer sheets for it.
            questions_list = []
            same_for_group = values['-GROUP-Q-']
            attempt = str(values['-ATTEMPTCOMBO-'])
            topic = values['-TEST-TOPIC-']
            letters = ('    A) ', '    B) ', '    C) ', '    D) ', '    E) ')
            questions_pdf_list = []
            if values['LANG-HU'] == True:
                test_lang = 'hu'
            else:
                test_lang = 'en'
            
            # Parsing questions from disk
            for db in range(0,qlines+1):
                question_database = values[f'-QDB{db}-']
                question_number = int(values[f'-QNO{db}-'])
                db_filename = os.path.basename(values[f'-QDB{db}-'])
                question_category, question_extension = os.path.splitext(db_filename)
                if os.path.isfile(question_database):
                    try: # to debug create_test.py, try running it with the problematic file
                        questions = parse_questions_xml(question_database)
                        questions_list.append({'questions': questions, 'number': question_number, 'category': question_category})
                    except:
                        try:
                            questions = parse_questions_txt(question_database)
                            questions_list.append({'questions': questions, 'number': question_number, 'category': question_category})
                        except:
                            print(question_database, local_text['errors']['questions_notparsed'])
            
            # Checking if the number of questions exceeds 60 (physical limit of the answer sheet)
            questions_ready = True
            students_ready = False
            database_ready = False
            test_length = 0
            if questions_list != []:
                for question_db in questions_list:
                    test_length = test_length + question_db['number']
                    #print(question_db)
                if test_length < 61:
                    pass
                else:
                    print(local_text['errors']['60_line1'], test_length)
                    print(local_text['errors']['60_line2'])
                    questions_ready = False
            else:
                print(local_text['errors']['no_questiondb'])
                
            for category in questions_list:
                if len(category['questions']) < category['number']:
                    print(local_text['errors']['qdb_toomany'], category['category'])
                    questions_ready = False
            
            if questions_ready == True:
                try:
                    students = parse_studentdata(values['-STUDENTS-WS-'], int(values['-EXTRATESTS-']))
                    students_location = os.path.dirname(os.path.abspath(values['-STUDENTS-WS-']))
                    students_ready = True
                    print(students)
                except:
                    print(local_text['errors']['students_notparsed'])
            
                solution_outfile = str(values['-SOLUTIONS-NEWOUTFILE-'])+'.txt'
                solution_outfile_mode = 'w'
                database_ready = True
            
            # Generating individual question lists for each student or group
            if questions_ready == True and students_ready == True and database_ready == True and test_length > 0:
                sg.popup_animated(sg.DEFAULT_BASE64_LOADING_GIF, background_color='white', time_between_frames=100)
                print(local_text['testcreation']['start'])
                print()
                
                for attempt in range(1,1+values['-ATTEMPTCOMBO-']):
                    if same_for_group == True:
                        groups = []
                        for student in students:
                            if student['group'] in groups:
                                pass
                            else:
                                groups.append(student['group'])
                                
                        group_questions_list = []
                        for group in groups:
                            selected_questions = []
                            for category in questions_list:
                                question_numbers = random.sample(range(0, len(category['questions'])), category['number'])
                                category_question_texts = get_questions_texts(category['questions'], question_numbers)
                                for question in category_question_texts:
                                    selected_questions.append(question)
                            group_questions_list.append({'group': group, 'questions': selected_questions})
                            
                    else:
                        student_questions_list = []
                        for student in students:
                            selected_questions = []
                            for category in questions_list:
                                question_numbers = random.sample(range(0, len(category['questions'])), category['number'])
                                category_question_texts = get_questions_texts(category['questions'], question_numbers)
                                for question in category_question_texts:
                                    selected_questions.append(question)
                            student_questions_list.append({'student': student['name'], 'questions': selected_questions})
                    
                    # Generating test and solution
                    solution_output = []
                    
                    # safer to have the user create an empty folder BEFOREHAND than attempting to write somewhere automatically
                    os.chdir(students_location)
                    
                    for student in range(len(students)):
                        lines_to_print = []
                        lines_to_solution = []
                        lines_to_solution.append('[BEGIN]')
                        lines_to_solution.append(students[student]['name']+','+students[student]['code']+','+str(students[student]['group'])+','+topic+','+str(attempt)+','+str(date.today()))
                        if same_for_group == True:
                            for group in range(len(group_questions_list)):
                                if group_questions_list[group]['group'] == students[student]['group']:
                                    questions_texts = group_questions_list[group]['questions']
                        else:
                            questions_texts = student_questions_list[student]['questions']
                        random.shuffle(questions_texts)
                        for j in range(len(questions_texts)):
                            newline = str(j+1)+'/ '+questions_texts[j]['q']
                            solution_line = str(j+1)+','
                            options = questions_texts[j]['a']
                            random.shuffle(options)
                            lines_to_print.append(newline)
                            letter_count = 0
                            for i in range(len(options)):
                                newline = letters[letter_count]+str(options[i]['option'])#+' '+str(options[i]['answer'])
                                solution_line = solution_line + str(options[i]['answer'])
                                if i < len(options)-1:
                                    solution_line = solution_line + ','
                                lines_to_print.append(newline)
                                letter_count = letter_count + 1
                            lines_to_print.append('')
                            lines_to_solution.append(solution_line)
                        lines_to_solution.append('[END]')
                        # Adding this solution to the rest
                        for line in lines_to_solution:
                            #print(line)
                            solution_output.append(line)
                        
                        # Creating PDF with questions
                        if test_lang == 'en':
                            if students[student]['group'] != '':
                                print_group = ', Group '+str(students[student]['group'])
                            else:
                                print_group = ''
                            thirdline = 'Multiple choice questions. Please, mark your answers on the answer sheet.'
                        elif test_lang == 'hu':
                            if students[student]['group'] != '':
                                print_group = ', '+str(students[student]['group'])+'. csoport'
                            else:
                                print_group = ''
                            thirdline = 'Többszörös választásos kérdések. Kérjük, válaszait az értékelőlapon jelölje!'
                        
                        elif test_lang == 'de':
                             if students[student]['group'] != '':
                                print_group = ', Gruppe '+str(students[student]['group'])
                             else:
                                print_group = ''
                             thirdline = 'Fragen mit mehreren Antworten. Bitte kreuzen Sie Ihre Antworten auf dem Antwortbogen an.'
                        
                        else:
                            print_group = str(students[student]['group'])
                        
                        if students[student]['code'] != '':
                            firstline = str(students[student]['name']) +' '+ students[student]['code'] + print_group
                        else:
                            firstline = str(students[student]['name']) + print_group
                        secondline = str(topic) +' ('+ str(attempt) + ')' +'      '+ str(date.today())
                        
                        pdf = FPDF(format='A4')
                        #pdf = fpdf.FPDF(format='A4')
                        font_bold = fm.findfont(fm.FontProperties(family='sans',weight='semibold'))
                        font_regular = fm.findfont('sans')
                        pdf.add_page()
                        pdf.add_font('Bold', 'B', font_bold, uni=True)
                        pdf.add_font('Regular', '', font_regular, uni=True)
                        pdf.set_font('Bold', 'B', size=9)
                        pdf.write(4,str(firstline))
                        pdf.ln()
                        pdf.write(4,str(secondline))
                        pdf.ln()
                        pdf.set_font("Regular",size=9)
                        pdf.write(4,'-----------------------------------------------------------------------------')
                        pdf.ln()
                        pdf.write(4,str(thirdline))
                        pdf.ln()
                        pdf.ln()
                        pdf.ln()
                        linecount = 5
                        for line in range(len(lines_to_print)):
                            if lines_to_print[line] == '':
                                if linecount > 60:
                                    for i in range(0,7):
                                        pdf.ln()
                                    linecount = 1
                            #print(lines_to_print[line])
                            pdf.write(4,str(lines_to_print[line]))
                            pdf.ln()
                            linecount = linecount + 1
                        
                        # if the number of pages is not divisible by 2, add extra lines
                        # this is needed to enable double-sided printing when answer sheets are bundled to it
                        for pair in ((0,11),(20,31),(40,51)):
                            pdf.set_text_color(255,255,255)
                            if pair[0] < test_length < pair[1]:
                                for i in range(60+pair[1]*6-test_length*6):
                                    pdf.write(4,str(i))
                                    pdf.ln()
                        outfile_pdf = 'Test_'+str(student)+'_'+str(attempt)+'.pdf'
                        pdf.output(outfile_pdf, 'F')
                        questions_pdf_list.append(outfile_pdf)
                    
                    # Generating complete solution file
                    if attempt == 1:
                        outfile = open(str(solution_outfile), solution_outfile_mode, encoding = 'UTF-16')
                    else:
                        outfile = open(str(solution_outfile), 'a', encoding = 'UTF-16')

                    for line in solution_output:
                        outfile.write(line+'\n')
                    outfile.close()
                    #window['-SOLUTIONS-NEWOUTFILE-'].update(solution_outfile)
                
                answer_sheet = create_answer_sheets(solution_outfile, test_lang, values['-SALT-'], questions_pdf_list)
                sg.popup_animated(None)
                print(local_text['success']['creation_line1'])
                print(local_text['success']['creation_line2'])
                print(solution_outfile)
                print()
                window['-PRINTDIR-'].update(students_location)
                os.chdir(script_location)
                window.write_event_value('-PRINTDIR-UPDATE-','')
            
            else:
                print() # to leave a line after any number of error messages
            
        elif event == '-PRINTDIR-UPDATE-':
            folder_printfiles = []
            for file in os.listdir(values['-PRINTDIR-']):
                if str(file)[-4:] == '.pdf' or str(file)[-4:] == '.PDF':
                    folder_printfiles.append(file)
            folder_printfiles.sort()
            chosen_printfiles = []
            window['-FOLDER-PRINTFILES-'].update(folder_printfiles)
            window['-CHOSEN-PRINTFILES-'].update(chosen_printfiles)
            
        elif event == '-FOLDER-PRINTFILES-':
            if folder_printfiles != []:
                chosen_printfiles.append(values['-FOLDER-PRINTFILES-'][0])
                window['-CHOSEN-PRINTFILES-'].update(values=chosen_printfiles)

        elif event == '-CHOSEN-PRINTFILES-':
            try:
                chosen_printfiles.remove(values['-CHOSEN-PRINTFILES-'][0])
            except IndexError:
                pass
            window['-CHOSEN-PRINTFILES-'].update(values=chosen_printfiles)
        
        elif event == '-FILTER1-' or event == '-FILTER2-' or event == '-FILTER3-' or event == '-FILTER4-':
            attempt_number = event[-2:-1]
            filtered_printfiles = []
            for file in folder_printfiles:
                if file[-6:] == "_"+attempt_number+".pdf" or file[-6:] == "_"+attempt_number+".PDF":
                    filtered_printfiles.append(file)
            window['-FOLDER-PRINTFILES-'].update(values=filtered_printfiles)
        
        elif event == '-FILTERADD-':
            for file in filtered_printfiles:
                chosen_printfiles.append(file)
            window['-CHOSEN-PRINTFILES-'].update(chosen_printfiles)
        
        elif event == '-FILTEROFF-':
            window['-FOLDER-PRINTFILES-'].update(values=folder_printfiles)
        
        elif event == '-RESET-PRINTFILES-':
            chosen_printfiles = []
            window['-CHOSEN-PRINTFILES-'].update(chosen_printfiles)
        
        elif event == '-PRINT-START-':
            if chosen_printfiles != []:
                os.chdir(values['-PRINTDIR-'])
                merger = PdfFileMerger()
                for file in chosen_printfiles:
                    merger.append(file)
                merged_name = 'PRINT_'+str(Path(values['-PRINTDIR-']).parts[-1])+'_'+str(date.today())+'.pdf'
                merger.write(merged_name)
                merger.close()
                print(merged_name)
                if sys.platform == "win32":
                    os.startfile(values['-PRINTDIR-']+'/'+merged_name)
                else:                                   # linux variants
                    subprocess.call(('xdg-open', values['-PRINTDIR-']+'/'+merged_name))
            else:
                pass                
        
        elif event == '-JUST-SHEETSDB-':
            # Creating answer sheets based on an already existing solution file.
            print(local_text['success']['answersheets_gen'])
            if values['LANG-HU'] == True:
                test_lang = 'hu'
            else:
                test_lang = 'en'
                
            solution_file = values['-CREATE-SOLUTIONDB-']
            
            final_filename = create_answer_sheets(solution_file, test_lang, attempt, values['-SALT-'])
            
            if final_filename == None:
                print(local_text['errors']['solution_notparsed'])
                print()
            else:
                print(local_text['success']['answersheets_done'])
                print(final_filename)
                print()
        
        elif event == '-SOLUTIONJOIN-':
            os.chdir(values['-SOLUTIONDIR-'])
            outfile_name = values['-SOLUTION-JOINED-FILE-']
            full_solution = []
            
            for file in chosen_solutionfiles:
                new_solution, students=ingest_solution(file,'')
                for line in range(len(new_solution)):
                    newline = ''
                    for entry in range(len(new_solution[line])):
                        if entry < len(new_solution[line])-1:
                            newline = newline+str(new_solution[line][entry])+','
                        else:
                            newline = newline+str(new_solution[line][entry])
                    full_solution.append(newline)

            outfile = open(outfile_name+'.txt', 'w', encoding = "UTF-16")
            for line in full_solution:
                outfile.write(line+'\n')
            outfile.close()   
        
        elif event == '-SOLUTIONDIR-UPDATE-':
            folder_solutionfiles = []
            for file in os.listdir(values['-SOLUTIONDIR-']):
                if str(file)[-4:] == '.txt' or str(file)[-4:] == '.txt':
                    folder_solutionfiles.append(file)
            folder_solutionfiles.sort()
            window['-FOLDER-SOLUTIONFILES-'].update(folder_solutionfiles)
            
        elif event == '-FOLDER-SOLUTIONFILES-':
            if folder_solutionfiles != []:
                chosen_solutionfiles.append(values['-FOLDER-SOLUTIONFILES-'][0])
                window['-CHOSEN-SOLUTIONFILES-'].update(values=chosen_solutionfiles)

        elif event == '-CHOSEN-SOLUTIONFILES-':
            try:
                chosen_solutionfiles.remove(values['-CHOSEN-SOLUTIONFILES-'][0])
            except IndexError:
                pass
            window['-CHOSEN-SOLUTIONFILES-'].update(values=chosen_solutionfiles)
        
        elif event == '-SELECTALL-SOLUTIONFILES-':
             if folder_solutionfiles != []:
                for file in folder_solutionfiles:
                    chosen_solutionfiles.append(file)
                window['-CHOSEN-SOLUTIONFILES-'].update(values=chosen_solutionfiles)
            
        elif event == '-RESET-SOLUTIONFILES-':
            chosen_solutionfiles = []
            window['-CHOSEN-SOLUTIONFILES-'].update(chosen_solutionfiles)
        
        elif event == '-ADD-QLINE-':
            qlines = qlines + 1
            window.extend_layout(window['QFRAME'],[
                [sg.Frame(local_text['questions']['db']+str(qlines+1),[
                [sg.Slider(range = (0,60), key = f'-QNO{qlines}-', default_value = 20,resolution = 1, orientation = 'h', size=(20,8)),
                 sg.InputText(os.getcwd()+'', key= f'-QDB{qlines}-',size=(70,8)),sg.FileBrowse(local_text['browsebutton'])]])]
                ])

if __name__ == '__main__':
    main()
