# These functions identify test pages based on their QR codes or by manual entry.

import cv2
import PySimpleGUI as sg
from PIL import Image
from PIL import ImageEnhance
from PIL import ImageFilter
from show_images import *
import os
import tempfile
import sys

def identify_test(original_file):
    pil_img = Image.open(original_file)
    detector = cv2.QRCodeDetector()
    width, height = pil_img.size
    
    # determining page orientation by cropping to quarters possibly having a QR
    topcrop_pil_img = pil_img.crop((width/2, 0, width, height/2))
    botcrop_pil_img = pil_img.crop((0, height/2, width/2, height))
    botcrop_pil_img = botcrop_pil_img.transpose(method=Image.ROTATE_180)
    
    paper_id = read_qr(topcrop_pil_img)
    if paper_id is not None:
        return paper_id, False
    
    paper_id = read_qr(botcrop_pil_img)
    if paper_id is not None:
        return paper_id, True
    
    paper_id = scale_contrast(topcrop_pil_img)
    if paper_id is not None:
        return paper_id, False
    
    paper_id = scale_contrast(botcrop_pil_img)
    if paper_id is not None:
        return paper_id, True
    
    return '', False

    
def read_qr(pil_img):
    temp_qr = tempfile.NamedTemporaryFile(suffix='.png').name
    pil_img.save(temp_qr)
    cv_img = cv2.imread(temp_qr)
    detector = cv2.QRCodeDetector()
    data, bbox, straight_qrcode = detector.detectAndDecode(cv_img)
    if bbox is not None:
        return data
    else:
        return None 
        
def scale_contrast(pil_image):
    contrast = 0.02
    while True:
        enh_con = ImageEnhance.Contrast(pil_image)
        pil_image_contrasted = (enh_con.enhance(contrast)) # might help with grading but breaks some QR codes
        #image_contrasted = image_contrasted.filter(ImageFilter.DETAIL)
        pil_image_contrasted = pil_image_contrasted.filter(ImageFilter.EDGE_ENHANCE_MORE)
        #image_contrasted = image_contrasted.filter(ImageFilter.SHARPEN)
        #contrasted_file = 'contrasted_'+original_file
        #image_contrasted.save(contrasted_file)
        #c_img = cv2.imread(contrasted_file)
        paper_id = read_qr(pil_image_contrasted)
        contrast = contrast + 0.02
        if paper_id != None:
            return paper_id
            
        if contrast > 2:
            break
        
def identify_test_manual(original_file, students, local_text):
    sg.ChangeLookAndFeel('Reddit')
    id_found = False
    
    if sys.platform == "win32":
        # OpenCV getting ready to show the image
        n_img = cv2.imread(original_file)
        width = round(n_img.shape[1]*0.5)
        height = round(n_img.shape[0]*0.5)
        n_img = cv2.resize(n_img, (width, height))
    else:
        # PIL showing the image
        image = Image.open(original_file)    
        image.show()
    
    while True:
        if sys.platform == "win32":
            show_images([local_text['image_title']], [n_img])
        text = sg.popup_get_text(local_text['popup_input'], location=(1000, 200))
        
        if text == None:
            return '', False
        
        else:
            namein = text
            if len(namein) > 0:
                matches = []
                # table components
                spacer = '              '
                name_local = local_text['name']
                test_local = local_text['test']
                name_spaced = spacer+name_local+spacer
                test_spaced = spacer+test_local+spacer
                table_headings = [name_spaced, test_spaced]
                table_data = []
                
                # populating the table
                for i in range(len(students)):
                    if namein.upper() == str(students[i]['name'])[:len(namein)].upper():
                        matches.append(students[i])
                    if namein == students[i]['hashed_id'][:len(namein)]:
                        matches.append(students[i])
                for match in matches:
                    table_data.append([match['name'], match['topic']+' ('+match['attempt']+')'])
                 
                if 1 < len(matches) < 20:
                    id_layout = [[sg.Text(local_text['matchwindow_message'])],
                                 [sg.Table(values=table_data,
                                   headings=table_headings,
                                   auto_size_columns=True,
                                   display_row_numbers=False,
                                   justification='center',
                                   key='-TABLE-',
                                   #enable_events = True
                                           )],
                                 [sg.Button('OK', size = (12,1),  button_color=('white', 'green'))]
                                 ]
                    id_window = sg.Window(local_text['matchwindow_title'], id_layout)
                    event, values = id_window.read()
                    if event == 'OK':
                        if values['-TABLE-'] != []:
                            table_selection = values['-TABLE-'][0]
                            selected_test = matches[table_selection]
                            paper_id = selected_test['hashed_id']
                            id_found = True
                            matches = [selected_test]
                            id_window.close()
                        else:
                            id_window.close()
                    
                elif len(matches) >= 20:
                    print(local_text['many_matches'])
                    print()
                
                elif len(matches) < 1:
                    print(local_text['no_match'])
                    print()
                else:
                    id_found = True
                    
            if id_found == True:
                print(local_text['success'])
                print(matches[0]['name'],'-',matches[0]['topic']+' ('+matches[0]['attempt']+')')
                print()
                paper_id = matches[0]['hashed_id']
                
                # Switch the comments below to get the popup asking about rotation
                #answer = sg.popup_yes_no("Was the page rotated?")
                answer = "No"
                
                if answer == "Yes":
                    return paper_id, True
                else:
                    return paper_id, False
                
                
def extra_to_name_manual(original_file, students, local_text):
    sg.ChangeLookAndFeel('Reddit')
    name_found = False
    
    if sys.platform == "win32":
        # OpenCV getting ready to show the image
        n_img = cv2.imread(original_file)
        width = round(n_img.shape[1]*0.5)
        height = round(n_img.shape[0]*0.5)
        n_img = cv2.resize(n_img, (width, height))
    else:
        # PIL showing the image
        image = Image.open(original_file)    
        image.show()
    
    while True:
        if sys.platform == "win32":
            show_images([local_text['image_title']], [n_img])
        text = sg.popup_get_text(local_text['popup_input'], location=(1000, 200))
        
        if text == None:
            return ''
        
        else:
            namein = text
            if len(namein) > 0:
                matches = []
                # table components
                spacer = '              '
                name_local = local_text['name']
                name_spaced = spacer+name_local+spacer
                table_headings = [name_spaced]
                table_data = []
                
                # populating the table
                for i in range(len(students)):
                    if namein.upper() == str(students[i]['name'])[:len(namein)].upper():
                        matches.append(students[i])
                        
                for match in matches:
                    if [match['name']] not in table_data:
                        table_data.append([match['name']])
                 
                if 1 < len(matches) < 20:
                    id_layout = [[sg.Text(local_text['matchwindow_message'])],
                                 [sg.Table(values=table_data,
                                   headings=table_headings,
                                   auto_size_columns=True,
                                   display_row_numbers=False,
                                   justification='center',
                                   key='-TABLE-',
                                   #enable_events = True
                                           )],
                                 [sg.Button('OK', size = (12,1),  button_color=('white', 'green'))]
                                 ]
                    
                    id_window = sg.Window(local_text['matchwindow_title'], id_layout)
                    event, values = id_window.read()
                    if event == 'OK':
                        if values['-TABLE-'] != []:
                            table_selection = values['-TABLE-'][0]
                            selected_test = matches[table_selection]
                            paper_id = selected_test['hashed_id']
                            name_found = True
                            matches = [selected_test]
                            id_window.close()
                        else:
                            id_window.close()
                    
                elif len(matches) >= 20:
                    print(local_text['many_matches'])
                    print()
                
                elif len(matches) < 1:
                    print(local_text['no_match'])
                    print()
                else:
                    name_found = True
                    
            if name_found == True:
                print(local_text['success'])
                print(matches[0]['name'],'-',matches[0]['topic']+' ('+matches[0]['attempt']+')')
                print()
                paper_id = matches[0]['name']
                
                # Switch the comments below to get the popup asking about rotation
                #answer = sg.popup_yes_no("Was the page rotated?")
                answer = "No"
                
                if answer == "Yes":
                    return paper_id
                else:
                    return paper_id

            
if __name__ == "__main__":
    print('Testing')