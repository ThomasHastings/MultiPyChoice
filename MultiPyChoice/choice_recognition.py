# These tools find the choices from the test page

import cv2
from pdf2image import convert_from_path
from hashlib import md5 as hashlib_md5
from imutils.perspective import four_point_transform
import numpy as np
from show_images import *

def cells(img):
    rows = np.vsplit(img, 36)
    cells = []
    for c in rows:
        cols = np.hsplit(c, 5)
        for cell in cols:
            cells.append(cell) 
    return cells

def boxes(cells, count_col, count_row, pixel_array):
    for box in cells:
        pixels = cv2.countNonZero(box) 
        pixel_array[count_row][count_col] = pixels
        count_col += 1
        if count_col == 5:
            count_row += 1
            count_col = 0

def handle_image(input_image):
    # declare some variables
    height = 800
    width = 600
    green = (0, 255, 0) # green color
    red = (0, 0, 255) # red color
    white = (255, 255, 255) # white color

    img = input_image
    img = cv2.resize(img, (width, height))
    img_copy = img.copy() # for display purposes
    img_copy1 = img.copy() # for display purposes

    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur_img = cv2.GaussianBlur(gray_img, (5, 5), 0)
    edge_img = cv2.Canny(blur_img, 10, 70)

    # find the contours in the image
    contours, _ = cv2.findContours(edge_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    # draw the contours
    cv2.drawContours(img, contours, -1, green, 3)
    #show_images(['image'], [img]) # helper function in helper.py file

    # Obtain the Top-Down View of the Document
    def get_rect_cnts(contours):
        rect_cnts = []
        for cnt in contours:
            # approximate the contour
            peri = cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
            # if the approximated contour is a rectangle ...
            if len(approx) == 4:
                # append it to our list
                rect_cnts.append(approx)
        # sort the contours from biggest to smallest
        rect_cnts = sorted(rect_cnts, key=cv2.contourArea, reverse=True)
        
        return rect_cnts

    rect_cnts = get_rect_cnts(contours)
    # warp perspective to get the top-down view of the document
    document = four_point_transform(img_copy, rect_cnts[0].reshape(4, 2))
    doc_copy = document.copy()  # for display purposes
    doc_copy1 = document.copy() # for display purposes

    cv2.drawContours(img_copy, rect_cnts, -1, green, 3)
    # helper function in helper.py file
    #show_images(['image', 'document'], [img_copy, document])

    # Find the Two Biggest Contours on the Document
    # find contours on the document
    gray_doc = cv2.cvtColor(document, cv2.COLOR_BGR2GRAY)
    blur_doc = cv2.GaussianBlur(gray_doc, (5, 5), 0)
    edge_doc = cv2.Canny(blur_doc, 10, 70)
    contours, _ = cv2.findContours(edge_doc, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    rect_cnts = get_rect_cnts(contours)
    # contours of the questions
    biggest_cnt = rect_cnts[0]
    # contours of the grade
    grade_cnt = rect_cnts[0]

    # draw the two biggest contours, which are the 
    # contour of the questions and the contour of the grade
    cv2.drawContours(document, rect_cnts[:2], -1, green, 3)
    #show_images(['two biggest contours'], [document])

    # region of interest
    cv2.imwrite('document.png',document)
    roi1 = document[28:615, 79:216]
    roi2 = document[28:615, 336:468]
    # roi1 = document[28:615, 85:222]
    # roi2 = document[28:615, 336:489] old
    try:
        roi1 = cv2.resize(roi1, (215,684))
        roi2 = cv2.resize(roi2, (215,684))
    except:
        return (False, None)


    # ROI HANDLE #

    roi1 = cv2.cvtColor(roi1, cv2.COLOR_BGR2GRAY)
    roi2 = cv2.cvtColor(roi2, cv2.COLOR_BGR2GRAY)
    roi1_thresh = cv2.threshold(roi1, 150, 255, cv2.THRESH_BINARY_INV)[1]
    roi2_thresh = cv2.threshold(roi2, 150, 255, cv2.THRESH_BINARY_INV)[1]
    #show_images(['roi1'], [roi1_thresh])
    cv2.imwrite('roi1.png',roi1)
    cv2.imwrite('roi2.png',roi2)
    
    # split image into cells
    cells1 = cells(roi1_thresh)
    cells2 = cells(roi2_thresh)
    
    # handle cells
    pixel_array1 = np.zeros((72,5))
    boxes(cells1, 0, 0, pixel_array1)
    boxes(cells2, 0, 36, pixel_array1)
    return (True, pixel_array1)

def find_choices(pixel_array, answer_treshold):
    answers_list = []
    raw_list = []
    for row in range(0,72):
        newline = []
        rawline = []
        for column in range(0,4):
            if pixel_array[row][column] > answer_treshold: # lazy-hands/no errors cutoff: 20-30; proper painter/errors skipped cutoff: 60-85
                newline.append('+')
                #print(pixel_array[row][column])
            else:
                newline.append('-')
                #print(pixel_array[row][column])
            rawline.append(pixel_array[row][column])
        answers_list.append(newline)
        raw_list.append(rawline)
    print(raw_list)
    return answers_list


if __name__ == '__main__':
    images = ["KimTaeyun.png","KassemAhmad.png"]