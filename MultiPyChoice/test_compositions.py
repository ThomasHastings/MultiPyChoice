def test_compositions():
    return [
            {'hu': 'Magyar szigorlati beugró',
           'en': 'Hungarian final exam',
           'language': 'hu',
           'topic': 'Farmakológia szigorlati beugró',
           'categories': [
                    {
                   'file': 'questions/m_ALT_2021_december.txt',
                   'number': 5,
                   'category': 'ALT'
                   },
                   {
                   'file': 'questions/m_VIR_2021_december.txt',
                   'number': 4,
                   'category': 'VIR'
                   },
                    {
                   'file': 'questions/m_gluko_asthma_helyi_antihiszt_izomrelax_2021_december.txt',
                   'number': 5,
                   'category': 'GALA'
                   },
                   {
                   'file': 'questions/m_NSAID_2021_december.txt',
                   'number': 1,
                   'category': 'NSAID'
                   },
                   {
                   'file': 'questions/m_antibiotikum_2021_december.txt',
                   'number': 3,
                   'category': 'AB'
                   },
                   {
                   'file': 'questions/m_virus_gomba_tbc_fereg_2021_december.txt',
                   'number': 3,
                   'category': 'VGT'
                   },
                   {
                   'file': 'questions/m_KIR_2021_december.txt',
                   'number': 10,
                   'category': 'KIR'
                   },
                    {
                   'file': 'questions/m_KVR_2021_december.txt',
                   'number': 12,
                   'category': 'KVR'
                   },
                    {
                   'file': 'questions/m_GIT_2021_december.txt',
                   'number': 3,
                   'category': 'GIT'
                   },
                   {
                   'file': 'questions/m_toxikologia_2021_december.txt',
                   'number': 2,
                   'category': 'TOXI'
                   },
                   {
                   'file': 'questions/m_hormon_immun_daganat_2021_december.txt',
                   'number': 2,
                   'category': 'HID'
                   } 
               ]
           },
            
           {'hu': 'Magyar kollokvium',
           'en': 'Hungarian exam',
           'topic': 'Farmakológia I. kollokvium',
           'language': 'hu',
           'categories': [
                   {
                   'file': 'questions/m_ALT_2021_december.txt',
                   'number': 7,
                   'category': 'ALT'
                   },
                   {
                   'file': 'questions/m_VIR_2021_december.txt',
                   'number': 7,
                   'category': 'VIR'
                   },
                   {
                   'file': 'questions/m_gluko_asthma_helyi_antihiszt_izomrelax_2021_december.txt',
                   'number': 7,
                   'category': 'GALA'
                   },
                    {
                   'file': 'questions/m_NSAID_2021_december.txt',
                   'number': 2,
                   'category': 'NSAID'
                   },
                   {
                   'file': 'questions/m_antibiotikum_2021_december.txt',
                   'number': 4,
                   'category': 'AB'
                   },
                   {
                   'file': 'questions/m_virus_gomba_tbc_fereg_2021_december.txt',
                   'number': 3,
                   'category': 'VGT'
                   }
               ]
               },
            {'hu': 'Magyar ált.farm demó',
           'en': 'Hungarian GPH MTO',
           'topic': 'Ált. farmakológia demonstráció',
           'language': 'hu',
           'categories': [
                   {
                   'file': 'questions/m_ALT_2021_december.txt',
                   'number': 50,
                   'category': 'KIR'
                   }
               ]
           }, 
            
            {'hu': 'Magyar VIR demó',
           'en': 'Hungarian ANS MTO',
           'topic': 'VIR demonstráció',
           'language': 'hu',
           'categories': [
                   {
                   'file': 'questions/m_VIR_2021_december.txt',
                   'number': 50,
                   'category': 'KIR'
                   }
               ]
           }, 

            {'hu': 'Magyar KIR demó',
           'en': 'Hungarian CNS MTO',
           'topic': 'KIR demonstráció',
           'language': 'hu',
           'categories': [
                   {
                   'file': 'questions/m_KIR_2021_december.txt',
                   'number': 50,
                   'category': 'KIR'
                   }
               ]
           }, 
            
            {'hu': 'Magyar KVR demó',
           'en': 'Hungarian CVS MTO',
           'topic': 'KVR demonstráció',
           'language': 'hu',
           'categories': [
                   {
                   'file': 'questions/m_KVR_2021_december.txt',
                   'number': 50,
                   'category': 'KVR'
                   }
               ]
           },
           
           {'hu': 'Angol szigorlati beugró',
           'en': 'English final exam',
           'language': 'en',
           'topic': 'Final exam - written part',
           'categories': [
                    {
                   'file': 'questions/a_GPH_2021_november.txt',
                   'number': 5,
                   'category': 'GPH'
                   },
                   {
                   'file': 'questions/a_ANS_2021_december.txt',
                   'number': 4,
                   'category': 'ANS'
                   },
                    {
                   'file': 'questions/a_gluco_asthma_local_antihist_musclerelax_2021_december.txt',
                   'number': 5,
                   'category': 'GALA'
                   },
                   {
                   'file': 'questions/a_NSAID_2021_december.txt',
                   'number': 1,
                   'category': 'NSAID'
                   },
                   {
                   'file': 'questions/a_antibiotics_2021_december.txt',
                   'number': 3,
                   'category': 'AB'
                   },
                   {
                   'file': 'questions/a_virus_fungi_tbc_helmith_2021_december.txt',
                   'number': 3,
                   'category': 'VFT'
                   },
                   {
                   'file': 'questions/a_CNS_2021_december.txt',
                   'number': 10,
                   'category': 'CNS'
                   },
                    {
                   'file': 'questions/a_CVS_2021_december.txt',
                   'number': 12,
                   'category': 'CVS'
                   },
                    {
                   'file': 'questions/a_GIT_2021_december.txt',
                   'number': 3,
                   'category': 'GIT'
                   },
                   {
                   'file': 'questions/a_toxicology_2021_december.txt',
                   'number': 2,
                   'category': 'TOXI'
                   },
                   {
                   'file': 'questions/a_hormone_immune_cancer_2021_december.txt',
                   'number': 2,
                   'category': 'HIC'
                   } 
               ]
           },

        {'hu': 'Angol kollokvium',
           'en': 'English exam',
           'language': 'en',
           'topic': 'Pharmacology I. exam',
           'categories': [
                   {
                   'file': 'questions/a_GPH_2021_november.txt',
                   'number': 6,
                   'category': 'GPH'
                   },
                   {
                   'file': 'questions/a_ANS_2021_december.txt',
                   'number': 8,
                   'category': 'ANS'
                   },
                   {
                   'file': 'questions/a_gluco_asthma_local_antihist_musclerelax_2021_december.txt',
                   'number': 7,
                   'category': 'GALA'
                   },
                    {
                   'file': 'questions/a_NSAID_2021_december.txt',
                   'number': 2,
                   'category': 'NSAID'
                   },
                   {
                   'file': 'questions/a_antibiotics_2021_december.txt',
                   'number': 4,
                   'category': 'AB'
                   },
                   {
                   'file': 'questions/a_virus_fungi_tbc_helmith_2021_december.txt',
                   'number': 3,
                   'category': 'VFT'
                   }
               ]
           },
            
             {'hu': 'Angol GPH demó',
           'en': 'English GPH MTO',
           'topic': 'General pharmacology midterm exam',
           'language': 'en',
           'categories': [
                   {
                   'file': 'questions/a_GPH_2021_november.txt',
                   'number': 50,
                   'category': 'CNS'
                   }
               ]
           },
            
             {'hu': 'Angol ANS demó',
           'en': 'English ANS MTO',
           'topic': 'ANS midterm exam',
           'language': 'en',
           'categories': [
                   {
                   'file': 'questions/a_ANS_2021_december.txt',
                   'number': 50,
                   'category': 'CNS'
                   }
               ]
           },
            
            {'hu': 'Angol CNS demó',
           'en': 'English CNS MTO',
           'topic': 'CNS midterm exam',
           'language': 'en',
           'categories': [
                   {
                   'file': 'questions/a_CNS_2021_december.txt',
                   'number': 50,
                   'category': 'CNS'
                   }
               ]
           },
            
           {'hu': 'Angol CVS demó',
           'en': 'English CVS MTO',
           'topic': 'CVS midterm exam',
           'language': 'en',
           'categories': [
                   {
                   'file': 'questions/a_CVS_2021_december.txt',
                   'number': 50,
                   'category': 'CVS'
                   }
               ]
           }
           ]