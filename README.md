# What is MultiPyChoice? #
MultiPyChoice is a tool for educators, enabling the creation of multiple choice tests and automated evaluation of these tests utilizing computer vision.

# How does it work? #
MultiPyChoice uses simple text files containing multiple choice questions (sample files included). It uses these text files to generate **individual** and **completely randomized** tests for every student. Student data is read from a spreadsheet (Excel sheet, sample file indlucded). All you need to do is to print the generated PDF, and hand the pages out to your students. They should mark their answers on their answer sheets. Then, scan the answer sheets only, and let MultiPyChoice identify and evaluate each sheet. In the end, you will receive the results in a spreadsheet.

# Note #
This tool is a work in progress. So is the documentation. Installation is not complicated, but not exactly straightforward either. Nevertheless, it will not break your machine. So, if you are interested, it may well be worth the 10 minutes it takes.

# Installation #
Due to a bug, `opencv-python` cannot be packaged into binary executables using `pyinstaller`. Therefore, a complete `Python 3` environment is needed to use MultiPyChoice. Furthermore, only cloning this `git` will not be enough to get started, because non-code components are also required. Therefore, complete releases can be found in the [Downloads](https://bitbucket.org/thomashastings/multipychoice/downloads/) section of this repository.

### Linux ###
1. Use your package manager to install the `python3` and `python3-tk`/`python3-tkinter` packages.
2. Then run the following code to install dependencies
	`python3 -m pip install --upgrade scipy pypdf2 qrcode opencv-python pdf2image imutils numpy pillow openpyxl pysimplegui fpdf`
3. Download and unpack the latest version of MultiPyChoice from [here](https://bitbucket.org/thomashastings/multipychoice/downloads/)
4. Open a terminal in the location where you unpacked the files and type `python3 multipy_choice.py`

### Windows ###
1. Download the [Python 3 installer](https://www.python.org/ftp/python/3.9.8/python-3.9.8-amd64.exe), and run it. Make sure to tick the 'Add Python 3.X to PATH' option.
2. Download and unpack the latest version of MultiPyChoice from [here](https://bitbucket.org/thomashastings/multipychoice/downloads/)
3. Run the `win_install.bat` file from the MultiPyChoice directory. This will take care of the `Python` dependencies.
4. Now you can run MultiPyChoice by double-clicking `multipy_choice.py`

### MacOS ###
1. [Install brew](https://brew.sh/)
2. Open a terminal and run
	`brew install python3 python-tk`
3. Now install python dependencies by running 
	`python3 -m pip install --upgrade scipy pypdf2 qrcode opencv-python pdf2image imutils numpy pillow openpyxl pysimplegui fpdf`
4. Download and unpack the latest version of MultiPyChoice from [here](https://bitbucket.org/thomashastings/multipychoice/downloads/)
5. Open a terminal in the folder of MultiPyChoice and run it with `python3 multipy_choice.py`


# Credit #
The system is based on two amazing tutorials by [Rouizi](https://github.com/Rouizi/bubble-sheet-multiple-choice-test-with-opencv) and [SullyChen](https://github.com/SullyChen/Automatic-Grading-OpenCV-Python).
GUI is provided by [PySimpleGUI](https://pypi.org/project/PySimpleGUI/).

# Disclaimer #
This software is provided as is under the GPLv3 license, without any warranties. No user-related data is logged or processed.
The Liberation Sans font is provided under the SIL Open Font License.
Bug reports and feautre requests are encouraged via the provided e-mail address, but pull requests are not accepted at this time.


# Contact #
You can reach the developer of this project at: thomas.hastings@tutanota.com